package br.com.inter.digitoUnico.strategy;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class DigitoUnicoStrategy {
	
	public Integer calcular(String digito, Integer concatenacao) {
		if (StringUtils.isBlank(digito) && concatenacao == null) {
			return null;
		} else if (concatenacao == null || concatenacao <= 0) {
			return somarDigitos(new BigInteger(digito));
		} 
		
		String digitoConcatenado = String.valueOf("");
		
		for (int i = 0; i < concatenacao; i++) {
			digitoConcatenado = digitoConcatenado.concat(digito);
		}
		
		return somarDigitos(new BigInteger(digitoConcatenado));
	}

	private static Integer somarDigitos(BigInteger digitoConcatenado) {
		List<BigInteger> digitos = integerToList(digitoConcatenado);
		BigInteger result = digitos.stream().reduce(BigInteger.ZERO, (subtotal, digito) -> subtotal.add(digito));
		if (new BigInteger("9").compareTo(result) < 0) {
			return somarDigitos(result);
		}
		
		return result.intValue();
	}

	private static List<BigInteger> integerToList(BigInteger digitoConcatenado) {
		List<BigInteger> digitos = new ArrayList<>();
		char[] charDigitos = digitoConcatenado.toString().toCharArray();
		for (char charDigito : charDigitos) {
			String algarismo = String.valueOf(charDigito);
			digitos.add(new BigInteger(algarismo));
		}
		
		return digitos;
	}

}
