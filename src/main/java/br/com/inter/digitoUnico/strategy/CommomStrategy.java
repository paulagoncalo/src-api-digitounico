package br.com.inter.digitoUnico.strategy;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import br.com.inter.digitoUnico.dto.DigitoUnicoDTO;
import br.com.inter.digitoUnico.service.CriptografiaService;

@Component
public class CommomStrategy {
	
	@Autowired
	private static CriptografiaService criptografiaService;
	
	public ResponseEntity<String> validarParametros(DigitoUnicoDTO dto) {
		List<String> erros = listarErrosParam(dto);
		if (!CollectionUtils.isEmpty(erros)) {
			String body = "";
			erros.forEach(body::concat);
			return ResponseEntity.badRequest().body(body);
		}

		return ResponseEntity.ok().body("OK");
	}

	public List<String> listarErrosParam(DigitoUnicoDTO dto) {
		List<String> list = new ArrayList<>();

		if (Objects.isNull(dto.getDigitoEntrada())) {
			list.add("O dígito não pode ser nulo!");
		}

		if (!StringUtils.isNumeric(dto.getDigitoEntrada())) {
			list.add("O dígito informado contém caracteres não numéricos");
		}

		BigInteger concatMax = BigInteger.TEN.pow(5);
		if (Objects.nonNull(dto.getVezesConcatenacao()) && (dto.getVezesConcatenacao() < 1
				|| (concatMax.compareTo(BigInteger.valueOf(dto.getVezesConcatenacao())) < 0))) {
			list.add("Concatenação excede o máximo(10^5).");
		}

		BigInteger tamanhoMax = BigInteger.TEN.pow(1000000);
		if ((new BigInteger(dto.getDigitoEntrada()).compareTo(BigInteger.ONE) < 0)
				|| (tamanhoMax.compareTo(new BigInteger(dto.getDigitoEntrada())) < 0)) {
			list.add("O dígito informado excede o máximo(10^^1000000).");
		}

		return list;
	}

	public ResponseEntity<String> validarCriptografiaUsuario(Long id, boolean isCriptografia) {
		List<String> erros = listarErrosCriptografia(id, isCriptografia);
		if (CollectionUtils.isEmpty(erros)) {
			String body = "";
			for (String string : erros) {
				body = body.concat(" ").concat(string);
			}
			return ResponseEntity.badRequest().body(body);
		}

		return ResponseEntity.ok().body("OK");
	}

	private static List<String> listarErrosCriptografia(Long id, boolean isCriptografia) {
		List<String> list = new ArrayList<>();
		if (Objects.isNull(id)) {
			list.add("O id usuário não pode ser nulo!");
		}

		if ((isCriptografia && getCriptografiaService().usuarioPossuiChave(id))) {
			list.add("O usuário já foi criptografado!");
		}
		
		if ((!isCriptografia && !getCriptografiaService().usuarioPossuiChave(id))) {
			list.add("O usuário ainda não foi criptografado!");
		}

		return list;
	}

	public static CriptografiaService getCriptografiaService() {
		return criptografiaService;
	}
	
	

}
