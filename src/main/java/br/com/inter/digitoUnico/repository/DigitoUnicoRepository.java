package br.com.inter.digitoUnico.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.inter.digitoUnico.entity.DigitoUnico;

public interface DigitoUnicoRepository extends JpaRepository<DigitoUnico, Long> {
	
	public List<DigitoUnico> getAllByUsuario(Long idUsuario);
}
