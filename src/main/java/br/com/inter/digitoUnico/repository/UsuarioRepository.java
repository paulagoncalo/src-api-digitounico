package br.com.inter.digitoUnico.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.inter.digitoUnico.entity.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long>{

	public List<Usuario> findAll();

	public Optional<Usuario> findById(Long id);

}
