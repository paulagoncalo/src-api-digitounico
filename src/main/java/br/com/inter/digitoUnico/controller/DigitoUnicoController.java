package br.com.inter.digitoUnico.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.inter.digitoUnico.converter.DigitoUnicoConverter;
import br.com.inter.digitoUnico.dto.DigitoUnicoDTO;
import br.com.inter.digitoUnico.service.DigitoUnicoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;

@Controller
@Api("DigitoUnico")
@RequestMapping("/api/digitounico")
public class DigitoUnicoController {

	@Autowired
	DigitoUnicoService digitoUnicoService;
	
	@Autowired
	DigitoUnicoConverter digitoUnicoConverter;
	
	@GetMapping("/all/usuario/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	@ApiOperation(httpMethod = "GET", 
		value = "Listar todos os dígitos de um usuário específico", 
		nickname = "getAllByUsuario", 
		tags = { "digitounico", })
	@ApiResponses(value = { 
		@ApiResponse(code = 200, message = "Lista de dígitos"),
		@ApiResponse(code = 204, message = "Nenhum dígito encontrado!"),
		@ApiResponse(code = 400, message = "Erro!")})
	@ResponseBody
	public List<DigitoUnicoDTO> getAllByUsuario (@PathVariable(value = "id") Long id) {
		List<DigitoUnicoDTO> list = digitoUnicoService.getAllByUsuario(id);
		return list;
	}
	
	@PostMapping("/calcular")
	@ApiOperation(httpMethod = "POST", 
		value = "Criar um dígito", 
		nickname = "createDigito", 
		tags = { "digitounico", })
	@ApiResponses(value = { 
		@ApiResponse(code = 201, message = "Dígito criado com sucesso"),
		@ApiResponse(code = 400, message = "Falha ao criar o dígito")})
	@ResponseBody
	public ResponseEntity<DigitoUnicoDTO> createDigito( @RequestBody DigitoUnicoDTO dto) {
		Integer digitoUnico = digitoUnicoService.calcularDigitoUnico(dto);
		DigitoUnicoDTO dtoCriado = digitoUnicoService.createDigito(dto, digitoUnico);
		return new ResponseEntity<>(dtoCriado, HttpStatus.CREATED);
	}
	
	@PostMapping("/calcular/usuario")
	@ApiOperation(httpMethod = "POST", 
		value = "Criar um dígito para um usuário específico", 
		nickname = "createDigitoByUsuario", 
		tags = { "digitounico", })
	@ApiResponses(value = { 
		@ApiResponse(code = 201, message = "Dígito criado com sucesso"),
		@ApiResponse(code = 400, message = "Falha ao criar o dígito")})
	@ResponseBody
	public ResponseEntity<DigitoUnicoDTO> createDigitoByUsuario( @RequestBody DigitoUnicoDTO dto) {
		Integer digitoUnico = digitoUnicoService.calcularDigitoUnico(dto);
		DigitoUnicoDTO dtoCriado = digitoUnicoService.createDigito(dto, digitoUnico);
		return new ResponseEntity<>(dtoCriado, HttpStatus.CREATED);
	}
}
