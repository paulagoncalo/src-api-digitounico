package br.com.inter.digitoUnico.controller;

import java.security.GeneralSecurityException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.server.ResponseStatusException;

import br.com.inter.digitoUnico.converter.UsuarioConverter;
import br.com.inter.digitoUnico.dto.UsuarioDTO;
import br.com.inter.digitoUnico.service.UsuarioService;
import br.com.inter.digitoUnico.strategy.CommomStrategy;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;


@Controller
@Api("Usuario")
@RequestMapping("/api/usuario")
public class UsuarioController {

	@Autowired
	UsuarioService usuarioService;

	@Autowired
	UsuarioConverter usuarioConverter;
	
	@Autowired
	CommomStrategy validatorStrategy;


	@PutMapping("/{id}/criptografar")
	@ApiOperation(httpMethod = "PUT", 
			value = "Criptografar os atributos do usuário.", 
			nickname = "criptografar", 
			tags = { "usuario", })
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Usuário criptografado com sucesso!"),
			@ApiResponse(code = 400, message = "Falha ao criptografar usuário") })
	@ResponseBody
	public ResponseEntity<UsuarioDTO> criptografarUsuario(@PathVariable(value = "id", required = true) Long id) throws GeneralSecurityException {
		ResponseEntity<String> response = validatorStrategy.validarCriptografiaUsuario(id, Boolean.TRUE);
		if (response.getStatusCode().equals(HttpStatus.BAD_REQUEST)) {
			throw new ResponseStatusException(response.getStatusCode(), response.getBody());
		}

		UsuarioDTO dto = new UsuarioDTO();
		dto = usuarioService.criptografar(id);
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}

	@PutMapping("/{id}/descriptografar")
	@ApiOperation(httpMethod = "PUT", 
			value = "Descriptografar os atributos do usuário.", 
			nickname = "descriptografar", 
			tags = { "usuario", })
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Usuário descriptografado com sucesso!"),
			@ApiResponse(code = 400, message = "Falha ao descriptografar usuário")})
	@ResponseBody
	public ResponseEntity<UsuarioDTO> descriptografarUsuario(
			@PathVariable(value = "id", required = true) Long id) {
		ResponseEntity<String> response = validatorStrategy.validarCriptografiaUsuario(id, Boolean.FALSE);
		if (response.getStatusCode().equals(HttpStatus.BAD_REQUEST)) {
			throw new ResponseStatusException(response.getStatusCode(), response.getBody());
		}

		UsuarioDTO dto = new UsuarioDTO();
		dto = usuarioService.descriptografar(id);
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}

}
