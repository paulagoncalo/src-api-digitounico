package br.com.inter.digitoUnico.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ErrorrHandlerController implements ErrorController {
	
	@GetMapping("/error")
	@ResponseBody
	public String customError() {
		return "The link you followed may be broken, or the page may have been removed.";
	}

	public String getErrorPath() {
		return "/error";
	}

}
