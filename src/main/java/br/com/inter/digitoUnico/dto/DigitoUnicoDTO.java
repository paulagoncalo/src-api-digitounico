package br.com.inter.digitoUnico.dto;

public class DigitoUnicoDTO {
	
	private Long id;
	private String digitoEntrada;
	private Integer vezesConcatenacao;
	private Integer digitoUnicoGerado;
	private Long idUsuario;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDigitoEntrada() {
		return digitoEntrada;
	}
	public void setDigitoEntrada(String digitoEntrada) {
		this.digitoEntrada = digitoEntrada;
	}
	public Integer getVezesConcatenacao() {
		return vezesConcatenacao;
	}
	public void setVezesConcatenacao(Integer vezesConcatenacao) {
		this.vezesConcatenacao = vezesConcatenacao;
	}
	public Integer getDigitoUnicoGerado() {
		return digitoUnicoGerado;
	}
	public void setDigitoUnicoGerado(Integer digitoUnicoGerado) {
		this.digitoUnicoGerado = digitoUnicoGerado;
	}
	public Long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	

}
