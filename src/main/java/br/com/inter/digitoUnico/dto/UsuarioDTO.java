package br.com.inter.digitoUnico.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Builder;

public class UsuarioDTO {
	
	@Builder.Default
	private Long id;
	private String nome;
	private String email;
	private List<DigitoUnicoDTO> digitoUnicoList = new ArrayList<>();
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<DigitoUnicoDTO> getDigitoUnicoList() {
		return digitoUnicoList;
	}
	public void setDigitoUnicoList(List<DigitoUnicoDTO> digitoUnicoList) {
		this.digitoUnicoList = digitoUnicoList;
	}
	
	

}
