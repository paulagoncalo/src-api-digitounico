package br.com.inter.digitoUnico.service;

import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;
import javax.management.openmbean.InvalidKeyException;

import org.springframework.stereotype.Service;

import br.com.inter.digitoUnico.entity.Usuario;

@Service
public class CriptografiaService {
	
	private static final String UTF8 = "UTF8";
	private static final String ALGORITMO_RSA = "RSA";
	private static final String ERROR_MESSAGE = "Erro ao gerar chaves de criptografia";
	private static final Map<Long, KeyPair> keys = new HashMap<>();
	private static final Decoder DECODER = Base64.getDecoder();
	private static final Encoder ENCODER = Base64.getEncoder();

	public void gerarChaves(Long idUsuario) throws GeneralSecurityException {
		try {
			KeyPairGenerator keyGen = KeyPairGenerator.getInstance(ALGORITMO_RSA);
			keyGen.initialize(2048);
			KeyPair keyPair = keyGen.generateKeyPair();
			keys.put(idUsuario, keyPair);
		} catch (Exception e) {
			throw new GeneralSecurityException(ERROR_MESSAGE);
		}
	}

	public Usuario criptografar(Usuario usuario) throws GeneralSecurityException {
		gerarChaves(usuario.getId());
		PublicKey chavePublica = keys.get(usuario.getId()).getPublic();
		usuario.setNome(criptografarTexto(usuario.getNome(), chavePublica));
		usuario.setEmail(criptografarTexto(usuario.getEmail(), chavePublica));
		return usuario;
	}

	private static String criptografarTexto(String texto, PublicKey chavePublica) {
		try {
			Cipher cipher = Cipher.getInstance(ALGORITMO_RSA);
			cipher.init(Cipher.ENCRYPT_MODE, chavePublica);
			byte[] textoBytes = cipher.doFinal(texto.getBytes());
			return ENCODER.encodeToString(textoBytes);
		} catch (Exception e) {
			throw new InvalidKeyException(ERROR_MESSAGE);
		}
	}

	public Usuario descriptografar(Usuario usuario) {
		PrivateKey chavePrivada = keys.get(usuario.getId()).getPrivate();
		usuario.setNome(descriptografarTexto(usuario.getNome(), chavePrivada));
		usuario.setEmail(descriptografarTexto(usuario.getEmail(), chavePrivada));
		keys.remove(usuario.getId());
		return usuario;
	}

	private static String descriptografarTexto(String textoCriptografado, PrivateKey chavePrivada) {
		try {
			Cipher cipher = Cipher.getInstance(ALGORITMO_RSA);
			cipher.init(Cipher.DECRYPT_MODE, chavePrivada);
			byte[] byteDecode = DECODER.decode(textoCriptografado);
			return new String(cipher.doFinal(byteDecode), UTF8);
		} catch (Exception e) {
			throw new InvalidKeyException(ERROR_MESSAGE);
		}
	}
	
	public boolean usuarioPossuiChave(Long idUsuario) {
		return !keys.isEmpty() && keys.containsKey(idUsuario);
	}

}
