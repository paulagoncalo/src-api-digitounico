package br.com.inter.digitoUnico.service;

import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.stereotype.Service;

@Service
public class CacheService {

	private static LinkedHashMap<String, Integer> cache = new LinkedHashMap<>();

	public void adicionar(String digito, Integer vezesConcatenacao, Integer digitoUnico) {
		if (cache.size() == 10) {
			removerChaveAntiga();
		}

		String chave = gerarNovaChave(digito, vezesConcatenacao);
		if (!procurarChave(chave)) {
			cache.put(chave, digitoUnico);
		}
	}

	public Integer buscar(String digito, Integer vezesConcatenacao) {
		Integer digitoUnico = null;
		String chave = gerarNovaChave(digito, vezesConcatenacao);
		if (procurarChave(chave)) {
			digitoUnico = cache.get(chave);
		}

		return digitoUnico;
	}

	private static String gerarNovaChave(String digito, Integer vezesConcatenacao) {
		return digito.concat("-").concat(String.valueOf(vezesConcatenacao));
	}

	private static void removerChaveAntiga() {
		String chaveAntiga = cache.keySet().stream().findFirst().orElse(null);
		cache.remove(chaveAntiga);
	}

	private static boolean procurarChave(String chave) {
		return cache.containsKey(chave);
	}

	public Set<Entry<String, Integer>> getCache() {
		return cache.entrySet();
	}

}
