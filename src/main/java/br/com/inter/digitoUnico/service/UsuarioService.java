package br.com.inter.digitoUnico.service;

import java.security.GeneralSecurityException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.inter.digitoUnico.converter.UsuarioConverter;
import br.com.inter.digitoUnico.dto.UsuarioDTO;
import br.com.inter.digitoUnico.entity.Usuario;
import br.com.inter.digitoUnico.repository.UsuarioRepository;

@Service
public class UsuarioService {
	
	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private UsuarioConverter usuarioConverter;
	
	@Autowired
	private CriptografiaService criptografiaService;
	
	public UsuarioDTO criptografar(Long id) throws GeneralSecurityException {
		Usuario usuario = getUsuarioRepository().findById(id).orElse(null);
		Usuario usuarioCriptografado = getUsuarioRepository().save(getCriptografiaService().criptografar(usuario));
		UsuarioDTO dto = getUsuarioConverter().convertToDTO(usuarioCriptografado);
		return dto;
	}

	public UsuarioDTO descriptografar(Long id) {
		Usuario usuarioCriptografado = getUsuarioRepository().findById(id).orElse(null);
		Usuario usuario = getUsuarioRepository().save(getCriptografiaService().descriptografar(usuarioCriptografado));
		UsuarioDTO dto = getUsuarioConverter().convertToDTO(usuario);
		return dto;
	}

	public UsuarioRepository getUsuarioRepository() {
		return usuarioRepository;
	}

	public void setUsuarioRepository(UsuarioRepository usuarioRepository) {
		this.usuarioRepository = usuarioRepository;
	}

	public UsuarioConverter getUsuarioConverter() {
		return usuarioConverter;
	}

	public void setUsuarioConverter(UsuarioConverter usuarioConverter) {
		this.usuarioConverter = usuarioConverter;
	}

	public CriptografiaService getCriptografiaService() {
		return criptografiaService;
	}

	public void setCriptografiaService(CriptografiaService criptografiaService) {
		this.criptografiaService = criptografiaService;
	}
	
	

}
