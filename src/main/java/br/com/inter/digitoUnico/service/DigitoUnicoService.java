package br.com.inter.digitoUnico.service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.inter.digitoUnico.converter.Converter;
import br.com.inter.digitoUnico.converter.DigitoUnicoConverter;
import br.com.inter.digitoUnico.dto.DigitoUnicoDTO;
import br.com.inter.digitoUnico.entity.DigitoUnico;
import br.com.inter.digitoUnico.repository.DigitoUnicoRepository;
import br.com.inter.digitoUnico.strategy.CommomStrategy;
import br.com.inter.digitoUnico.strategy.DigitoUnicoStrategy;

@Service
public class DigitoUnicoService {
	
	@Autowired
	private DigitoUnicoRepository digitoUnicoRepository;

	@Autowired (required = true)
	private DigitoUnicoConverter digitoUnicoConverter;

	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private CacheService cacheService;
	
	@Autowired
	private DigitoUnicoStrategy digitoUnicoStrategy;
	
	@Autowired
	private CommomStrategy validatorStrategy;

	
	protected Converter<DigitoUnico, DigitoUnicoDTO> getConverter() {
		return this.digitoUnicoConverter;
	}

	public List<DigitoUnicoDTO> getAllByUsuario(Long idUsuario) {
		List<DigitoUnico> entities = getDigitoUnicoRepository().getAllByUsuario(idUsuario);
		List<DigitoUnicoDTO> dtos = entities.parallelStream().map(entity -> getConverter().convertToDTO(entity))
				.collect(Collectors.toList());
		return dtos;
	}

	public DigitoUnicoDTO createDigito(DigitoUnicoDTO digitoUnicoDTO, Integer digitoUnico) {
		
		DigitoUnicoDTO dto  = new DigitoUnicoDTO();
		if (validarDigito(digitoUnicoDTO)) {
			dto = save(digitoUnicoDTO);
		}

		return dto;
	}

	public Integer calcularDigitoUnico(DigitoUnicoDTO dto) {
		ResponseEntity<String> response = getValidatorStrategy().validarParametros(dto);
		if (response.getStatusCode().equals(HttpStatus.BAD_REQUEST)) {
			throw new ResponseStatusException(response.getStatusCode(), response.getBody());
		}
		Integer digitoUnico = calcular(dto);

		return digitoUnico;
	}

	private Integer calcular(DigitoUnicoDTO dto) {
		Integer digitoUnico = getCacheService().buscar(dto.getDigitoEntrada(), dto.getVezesConcatenacao());
		if (Objects.isNull(digitoUnico)) {
			digitoUnico = getDigitoUnicoStrategy().calcular(dto.getDigitoEntrada(), dto.getVezesConcatenacao());
			getCacheService().adicionar(dto.getDigitoEntrada(), dto.getVezesConcatenacao(), digitoUnico);
		}
		return digitoUnico;
	}

	public boolean validarDigito(DigitoUnicoDTO digitoUnico) {
		if (Objects.isNull(digitoUnico)) {
			return Boolean.FALSE;
		}
		boolean result = Objects.nonNull(digitoUnico.getDigitoUnicoGerado()) && !getDigitoUnicoRepository().findAll().contains(getConverter().convertToEntity(digitoUnico));
		return result;
	}

	private DigitoUnicoDTO save(DigitoUnicoDTO dto) {	
		getDigitoUnicoRepository().save(getConverter().convertToEntity(dto));
		return dto;
	}

	public DigitoUnicoRepository getDigitoUnicoRepository() {
		return digitoUnicoRepository;
	}

	public void setDigitoUnicoRepository(DigitoUnicoRepository digitoUnicoRepository) {
		this.digitoUnicoRepository = digitoUnicoRepository;
	}

	public UsuarioService getUsuarioService() {
		return usuarioService;
	}

	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	public CacheService getCacheService() {
		return cacheService;
	}

	public void setCacheService(CacheService cacheService) {
		this.cacheService = cacheService;
	}

	public DigitoUnicoStrategy getDigitoUnicoStrategy() {
		return digitoUnicoStrategy;
	}

	public void setDigitoUnicoStrategy(DigitoUnicoStrategy digitoUnicoStrategy) {
		this.digitoUnicoStrategy = digitoUnicoStrategy;
	}

	public CommomStrategy getValidatorStrategy() {
		return validatorStrategy;
	}

	public void setValidatorStrategy(CommomStrategy validatorStrategy) {
		this.validatorStrategy = validatorStrategy;
	}
	
	

}
