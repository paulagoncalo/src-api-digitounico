package br.com.inter.digitoUnico.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "DIGITOUNICO")
public class DigitoUnico {
	
	@Id
	@Column(name = "ID", nullable = false)
	private Long id;
	
	
	@Column(name = "DIGITOENTRADA", nullable = false)
	private String digitoEntrada;
	
	@Column(name = "VEZESCONCATENACAO", nullable = true)
	private Integer vezesConcatenacao;
	
	@Column(name = "DIGITOUNICOGERADO", nullable = false)
	private Integer digitoUnicoGerado;
	
	@JoinColumn(name = "USUARIO", nullable = true)
	@ManyToOne(fetch = FetchType.LAZY)
	private Usuario usuario;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDigitoEntrada() {
		return digitoEntrada;
	}

	public void setDigitoEntrada(String digitoEntrada) {
		this.digitoEntrada = digitoEntrada;
	}

	public Integer getVezesConcatenacao() {
		return vezesConcatenacao;
	}

	public void setVezesConcatenacao(Integer vezesConcatenacao) {
		this.vezesConcatenacao = vezesConcatenacao;
	}

	public Integer getDigitoUnicoGerado() {
		return digitoUnicoGerado;
	}

	public void setDigitoUnicoGerado(Integer digitoUnicoGerado) {
		this.digitoUnicoGerado = digitoUnicoGerado;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((digitoEntrada == null) ? 0 : digitoEntrada.hashCode());
		result = prime * result + ((digitoUnicoGerado == null) ? 0 : digitoUnicoGerado.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		result = prime * result + ((vezesConcatenacao == null) ? 0 : vezesConcatenacao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DigitoUnico other = (DigitoUnico) obj;
		if (digitoEntrada == null) {
			if (other.digitoEntrada != null)
				return false;
		} else if (!digitoEntrada.equals(other.digitoEntrada))
			return false;
		if (digitoUnicoGerado == null) {
			if (other.digitoUnicoGerado != null)
				return false;
		} else if (!digitoUnicoGerado.equals(other.digitoUnicoGerado))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		if (vezesConcatenacao == null) {
			if (other.vezesConcatenacao != null)
				return false;
		} else if (!vezesConcatenacao.equals(other.vezesConcatenacao))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DigitoUnico [id=" + id + ", digitoEntrada=" + digitoEntrada + ", vezesConcatenacao=" + vezesConcatenacao
				+ ", digitoUnicoGerado=" + digitoUnicoGerado + ", usuario=" + usuario + "]";
	}
	
	

}
