package br.com.inter.digitoUnico.converter;

public interface Converter <T, DTO> {

	public T convertToEntity(DTO dto);

	public DTO convertToDTO(T entity);

}
