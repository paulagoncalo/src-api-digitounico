package br.com.inter.digitoUnico.converter;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import br.com.inter.digitoUnico.dto.UsuarioDTO;
import br.com.inter.digitoUnico.entity.Usuario;

@Component
public class UsuarioConverter implements Converter<Usuario, UsuarioDTO> {
	
	@Override
	public Usuario convertToEntity(UsuarioDTO dto) {
		Usuario entity = new Usuario();
		BeanUtils.copyProperties(dto, entity);
		return entity;
	}
	
	@Override
	public UsuarioDTO convertToDTO(Usuario entity) {
		UsuarioDTO dto = new UsuarioDTO();
		BeanUtils.copyProperties(entity, dto);
		return dto;
	}

}
