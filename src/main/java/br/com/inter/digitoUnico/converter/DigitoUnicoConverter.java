package br.com.inter.digitoUnico.converter;

import java.util.Objects;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.inter.digitoUnico.dto.DigitoUnicoDTO;
import br.com.inter.digitoUnico.entity.DigitoUnico;
import br.com.inter.digitoUnico.entity.Usuario;
import br.com.inter.digitoUnico.repository.UsuarioRepository;

@Component
public class DigitoUnicoConverter implements Converter<DigitoUnico, DigitoUnicoDTO> {

	@Autowired
	UsuarioRepository usuarioRepository;

	@Override
	public DigitoUnico convertToEntity(DigitoUnicoDTO dto) {
		DigitoUnico entity = new DigitoUnico();
		BeanUtils.copyProperties(dto, entity);
		if (Objects.nonNull(dto.getIdUsuario())) {
			Usuario usuario = usuarioRepository.getById(dto.getIdUsuario());
			entity.setUsuario(usuario);
		}
		return entity;
	}

	@Override
	public DigitoUnicoDTO convertToDTO(DigitoUnico entity) {
		DigitoUnicoDTO dto = new DigitoUnicoDTO();
		BeanUtils.copyProperties(entity, dto);
		if (Objects.nonNull(entity.getUsuario())) {
			dto.setIdUsuario(entity.getUsuario().getId());
		}
		return dto;
	}


}
